import {execFileSync} from "child_process";
import config from "./config.js";

class TermuxVolumeStatusEntry {
	/** @type {String} The stream's name */
	stream;
	/** @type {Number} the stream's current volume */
	volume;
	/** @type {Number} the stream's maximum volume */
	max_volume;
}
/**
 * @returns {TermuxVolumeStatusEntry[]} the volume response
 */
export function get_volume() {
	const res=JSON.parse(Buffer.from(execFileSync("termux-volume", [])).toString());
	/* console.log(res); */
	return res;
};
/**
 * @param {String} stream
 * @param {Number} volume
 */
export function set_volume(stream, volume) {
	/* This is the only thing keeping a user from executing termux-volume with an arbitrary first argument. Yes, throwing an error while handling an HTTP request is acceptable. */
	if(!config.volumeTypes.includes(stream)) throw new TypeError();
	if(volume!=Math.floor(volume)) throw new RangeError();
	execFileSync("termux-volume", [stream, volume])
}