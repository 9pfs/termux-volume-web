import express from "express";
import config from "./config.js";
import {get_volume, set_volume} from "./volume-util.js";
console.log(config);
const app = express();
app.set("view engine", "ejs");
app.get("/", function(req, res) {
    res.render("index", {data: get_volume()});
});
app.use("/update", express.urlencoded({
    extended: false
}));
app.post("/update", function(req, res) {
    set_volume(req.body.stream, req.body.volume);
    res.redirect(302, req.headers.referer);
});
app.listen(3507, "0.0.0.0");