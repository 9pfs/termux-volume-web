const config = {
    volumeTypes: [
        "call",
        "system",
        "ring",
        "music",
        "alarm",
        "notification"
    ]
};
export default config;